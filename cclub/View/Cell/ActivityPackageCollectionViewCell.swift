//
//  ActivityPackageCollectionViewCell.swift
//  cclub
//
//  Created by Mohamed Haseeb on 28/07/20.
//  Copyright © 2020 Mohamed Haseeb. All rights reserved.
//

import UIKit

class ActivityPackageCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.layer.borderColor = UIColor.darkGray.cgColor
               contentView.layer.cornerRadius = 2.0
               
              layer.shadowColor = UIColor.gray.cgColor
              layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
              layer.shadowRadius = 4.0
              layer.cornerRadius = 5.0
              layer.shadowOpacity = 0.5
               
               layer.masksToBounds = false
               contentView.clipsToBounds = true
        // Initialization code
    }

}
