//
//  ReusableListViewController.swift
//  cclub
//
//  Created by Mohamed Haseeb on 17/07/20.
//  Copyright © 2020 Mohamed Haseeb. All rights reserved.
//

import UIKit

class ReusableListViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    var delegate: didSelectPopDelegate?
    var dataSource: [String] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        cell?.textLabel?.text = dataSource[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.showValue(string: dataSource[indexPath.row])
        print(dataSource[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
