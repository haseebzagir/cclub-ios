//
//  LoginViewController.swift
//  cclub
//
//  Created by Mohamed Haseeb on 13/07/20.
//  Copyright © 2020 Mohamed Haseeb. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class LoginViewController: UIViewController {
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var emailField: SkyFloatingLabelTextFieldWithIcon!
    
    @IBOutlet weak var guestButton: UIButton!
    //MARK:- View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(gestureRecognizer)
        let gradient = CAGradientLayer()
        gradient.frame = mainImage.bounds
        gradient.colors = [UIColor.clear.cgColor,UIColor.white.cgColor]
        gradient.locations = [0,1]
        mainImage.layer.addSublayer(gradient)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardFrame = keyboardSize.cgRectValue
        if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= keyboardFrame.height * 0.5
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin = CGPoint(x: 0, y: 0)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
    }
    
    override func viewDidLayoutSubviews() {
        loginButton.layer.cornerRadius = 5.0
        guestButton.layer.cornerRadius = 5.0
}
       
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the Navigation Bar
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    //MARK:- Button Action
    @IBAction func continueAsGuest(_ sender: Any) {
        let homeStoryBoard = UIStoryboard(name: "Home", bundle: nil)
        guard let homeTabVC = homeStoryBoard.instantiateViewController(withIdentifier: "HomeTabVC") as? HomeTabViewController else { return }
        homeTabVC.modalPresentationStyle = .fullScreen
        present(homeTabVC, animated: true, completion: nil)
    }
    
    @objc func textFieldDidChange(_ textfield: UITextField) {
        if let text = textfield.text {
            if let floatingLabelTextField = textfield as? SkyFloatingLabelTextFieldWithIcon {
                if(text.count < 3 || !text.isValidEmail()) {
                    floatingLabelTextField.errorMessage = "Invalid email"
                    self.loginButton.backgroundColor = UIColor.gray
                    loginButton.isEnabled = false
                }
                else {
                    // The error message will only disappear when we reset it to nil or empty string
                    self.loginButton.backgroundColor = UIColor(displayP3Red: 36.0/255.0, green: 87.0/255.0, blue: 159.0/255.0, alpha: 1.0)
                    floatingLabelTextField.errorMessage = ""
                    loginButton.isEnabled = true
                }
            }
        }
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
    }
}


extension String {
    
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
}
