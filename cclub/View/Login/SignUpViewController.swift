//
//  SignUpViewController.swift
//  cclub
//
//  Created by Mohamed Haseeb on 17/07/20.
//  Copyright © 2020 Mohamed Haseeb. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol didSelectPopDelegate {
    func showValue(string: String)
}

class SignUpViewController: UIViewController, UITextFieldDelegate,UIPopoverPresentationControllerDelegate ,didSelectPopDelegate{

    @IBOutlet weak var signupButton: UIButton!
    
    @IBOutlet weak var signUpScrollView: UIScrollView!
    @IBOutlet weak var firstNameField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lastNameField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var phoneNumberField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var memberShipTyoe: SkyFloatingLabelTextField!
    
    @IBOutlet weak var membershipPlans: SkyFloatingLabelTextField!
    
    @IBOutlet weak var nationality: SkyFloatingLabelTextField!
    
    @IBOutlet weak var genderField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var professionField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var dateOfBirthField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var civilIdNumberField: SkyFloatingLabelTextField!
    
    private var datePicker = UIDatePicker()
    
    var textFieldType: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.datePickerMode = .date
        let toolbar = UIToolbar();
                 toolbar.sizeToFit()

                 //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(donedatePicker))
              let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
                 toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

              // add toolbar to textField
        dateOfBirthField.inputAccessoryView = toolbar
        dateOfBirthField.inputView = datePicker
        datePicker.maximumDate = Date()
        datePicker.minimumDate = Date(timeIntervalSince1970: TimeInterval())
        
//        datePicker.addTarget(self, action: #selector(SignUpViewController.dateChanged(datePicker:)), for: .valueChanged)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.viewTapped(gestureRecognizer:)))
       
        view.addGestureRecognizer(gestureRecognizer)
        
        firstNameField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        lastNameField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        emailField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        phoneNumberField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        professionField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        civilIdNumberField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        memberShipTyoe.text = "CClub Lifetime"

        self.signupButton.backgroundColor = UIColor.gray
        self.signupButton.layer.cornerRadius = 5.0
        self.signupButton.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
    @objc func cancelDatePicker() {
          //cancel button dismiss datepicker dialog
           self.view.endEditing(true)
         }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardFrame = keyboardSize.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardFrame.height, right: 0.0)
        
        self.signUpScrollView.contentInset = contentInsets
        self.signUpScrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardFrame.height
        if let activeFieldPresent = textFieldType
        {
            if (!aRect.contains(textFieldType!.frame.origin))
            {
                self.signUpScrollView.scrollRectToVisible(activeFieldPresent.frame, animated: true)
            }
        }
       }
       
       @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        signUpScrollView.contentInset = contentInset
        //        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        //        let keyboardFrame = keyboardSize.cgRectValue.size
        //        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        //        self.signUpScrollView.contentInset = contentInsets
        //        self.signUpScrollView.scrollIndicatorInsets = contentInsets
        //        self.view.endEditing(true)
        //        self.signUpScrollView.isScrollEnabled = false
//        guard let userInfo = notification.userInfo else {return}
       }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textFieldType = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumberField {
             let maxLength = 8
               let currentString: NSString = textField.text! as NSString
               let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
        } else {
            return true
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField)
    {
        textFieldType = nil
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textFieldType = textField
        if textField == memberShipTyoe {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ReusableTableView") as? ReusableListViewController
            vc?.delegate = self
            vc?.dataSource = ["Kid","Men"]
            
            self.navigationController?.present(vc!, animated: true, completion: nil)
            self.navigationController?.modalPresentationStyle = .formSheet
            return false
            
        } else if textField == membershipPlans {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ReusableTableView") as? ReusableListViewController
            vc?.delegate = self
            vc?.dataSource = ["1 Yr Gold","1 Yr Silver","1 Yr Platinum"]
            
            self.navigationController?.present(vc!, animated: true, completion: nil)
            self.navigationController?.modalPresentationStyle = .formSheet
            return false
        } else if textField == nationality {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ReusableTableView") as? ReusableListViewController
            vc?.delegate = self
            vc?.dataSource = ["United States", "Canada", "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and/or Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Cook Islands", "Costa Rica", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecudaor", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France, Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kosovo", "Kuwait", "Kyrgyzstan", "Lao People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfork Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia South Sandwich Islands", "South Sudan", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbarn and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States minor outlying islands", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City State", "Venezuela", "Vietnam", "Virigan Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zaire", "Zambia", "Zimbabwe"]
            
            self.navigationController?.present(vc!, animated: true, completion: nil)
            self.navigationController?.modalPresentationStyle = .formSheet
            
            return false
        }  else {
            return true
        }
    }
    
    func showValue(string: String) {
        switch textFieldType {
        case memberShipTyoe:
            self.memberShipTyoe.text = string
        case membershipPlans:
            self.membershipPlans.text = string
        case nationality:
            self.nationality.text = string
        default:
            return
        }
    }
    
    @objc func donedatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateOfBirthField.text = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }

    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @objc func textFieldDidChange(_ textfield: UITextField) {
        
        switch textfield {
        case firstNameField:
            firstNameField.errorMessage = validateFirstName(textField: firstNameField) ? "" : "first name cannot be empty"
            
        case emailField:
            emailField.errorMessage = emailField.text!.isValidEmail() ? "" : "Invalid email"
            
        case lastNameField:
            lastNameField.errorMessage = validateFirstName(textField: lastNameField) ? "" : "last name cannot be empty"
            
        case phoneNumberField:
            phoneNumberField.errorMessage = phoneNumberField.text!.count < 8 ? "Invalid Phone number": ""
            
        case professionField:
            professionField.errorMessage = validateFirstName(textField: professionField) ? "" : "profession cannot be empty"
            
        case civilIdNumberField:
            civilIdNumberField.errorMessage = validateFirstName(textField: civilIdNumberField) ? "" : "civil id number cannot be empty"
        default:
            return
        }
        
        if (firstNameField.text! == "" || lastNameField.text! == "" || phoneNumberField.text! == "" || professionField.text! == "" || civilIdNumberField.text! == "") {
            self.signupButton.backgroundColor = UIColor.gray
            self.signupButton.isEnabled = false
        } else {
            self.signupButton.backgroundColor = UIColor(displayP3Red: 36.0/255.0, green: 87.0/255.0, blue: 159.0/255.0, alpha: 1.0)
            self.signupButton.isEnabled = true
        }
          
    }
    
    
    func validateFirstName(textField: UITextField) -> Bool {
        guard let textFieldText = textField.text else {
            return false
        }
        return textFieldText.count == 0 ? false : true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


