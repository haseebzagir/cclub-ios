//
//  HomeViewController.swift
//  cclub
//
//  Created by Mohamed Haseeb on 13/07/20.
//  Copyright © 2020 Mohamed Haseeb. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var HomePageCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let df = DateFormatter()
        df.dateFormat = "MMM dd yyyy"
        let ff = df.string(from: Date())
        let label = UILabel()
        label.font = UIFont(name: "NexaDemo-Bold", size: 16.0)
        label.text = "Welcome"
        label.textColor = UIColor.white
        let label1 = UILabel()
        label1.font = UIFont(name: "NexaDemo-Bold", size: 16.0)
        label1.text = ff
        
        label1.textColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: label1)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: label)

        HomePageCollectionView.register(UINib(nibName: "ActivitiesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ActivitiesCell")

        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
              
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = HomePageCollectionView.dequeueReusableCell(withReuseIdentifier: "ActivitiesCell", for: indexPath) as? ActivitiesCollectionViewCell
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Did Select")
        let homeStoryBoard = UIStoryboard(name: "Home", bundle: nil)
        guard let homeTabVC = homeStoryBoard.instantiateViewController(withIdentifier: "SampleVC") as? ActivitiesGenericViewController else { return }
        self.navigationController?.pushViewController(homeTabVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeViewController : UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 10, right: 20)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.frame.size.width
        return CGSize(width: (collectionViewWidth-50)/2, height: (collectionViewWidth-50)/2)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}
