//
//  OptionsViewController.swift
//  cclub
//
//  Created by Mohamed Haseeb on 28/07/20.
//  Copyright © 2020 Mohamed Haseeb. All rights reserved.
//

import UIKit

class OptionsViewController: UIViewController, UITableViewDataSource {
    
    
    let dataSource = ["Profile Settings","Notification", "Rate the app", "Help and support", "Logout"]
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? UITableViewCell
        cell?.textLabel?.text = dataSource[indexPath.row]
        return cell!
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
