//
//  ActivitiesGenericViewController.swift
//  cclub
//
//  Created by Mohamed Haseeb on 28/07/20.
//  Copyright © 2020 Mohamed Haseeb. All rights reserved.
//

import UIKit


//struct Activities {
//    var imageName: String?
//
//}

class ActivitiesGenericViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate{
    
//    var dataSource: [String] = []
    
    @IBOutlet weak var activityPackageCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        activityPackageCollectionView.register(UINib(nibName: "ActivityPackageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ActivityPackageCollectionViewCell")
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 8
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = activityPackageCollectionView.dequeueReusableCell(withReuseIdentifier: "ActivityPackageCollectionViewCell", for: indexPath) as? ActivityPackageCollectionViewCell
            return cell!
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            print("Did Select")
//            let homeStoryBoard = UIStoryboard(name: "Home", bundle: nil)
//            guard let homeTabVC = homeStoryBoard.instantiateViewController(withIdentifier: "SampleVC") as? ActivitiesGenericViewController else { return }
//            self.navigationController?.pushViewController(homeTabVC, animated: true)
        }

        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
        }
        */

    }

    extension ActivitiesGenericViewController : UICollectionViewDelegateFlowLayout{

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20)
        }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let collectionViewWidth = collectionView.frame.size.width
            return CGSize(width: (collectionViewWidth-40), height: 200)
        }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 10
        }
    }
